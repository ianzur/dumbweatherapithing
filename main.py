import pathlib
import requests
import datetime
import pandas as pd
import socket

CONFIG_PATH = pathlib.Path.home() / '.config' / 'polybar'
CSV_NAME = 'weatherInfo.csv'
OFFICE = 'OUN'
GRID_COORDINATES = '97,95'
API_LOCATION = 'https://api.weather.gov/gridpoints/' + OFFICE + '/' + GRID_COORDINATES + '/forecast/hourly'

HOSTNAME = 'one.one.one.one'
PORT = 80
TIMEOUT = 2

def validConnection(hostname) -> bool:
    try:
        host = socket.gethostbyname(hostname)
        s = socket.create_connection((host, PORT), TIMEOUT)
        s.close()
        return True
    except Exception:
        pass
    return False 

def processWeatherData(df, csvLocation):
    df.to_csv(csvLocation)
    df[['startTime', 'endTime']] = df[['startTime', 'endTime']].apply(pd.to_datetime)

    currTime = datetime.datetime.now(datetime.timezone.utc)
    df = df[df.endTime > currTime]
    df = df[df.startTime < currTime]
    
    if df.empty:
        print("N/A")
        return

    temperature = df.temperature.item()
    temperatureUnit = df.temperatureUnit.item()
    chanceOfPrecipitation = df.probabilityOfPrecipitation.item()['value']
    print(f"{temperature}°{temperatureUnit} {chanceOfPrecipitation}%")

def getWeatherData(csvLocation) -> pd.DataFrame | None:
    df = None
    
    if validConnection(HOSTNAME):
        response = requests.get(API_LOCATION, headers={'Accept': 'application/geo+json'}, timeout=TIMEOUT)
        if response.status_code == 200:
            weatherInfo = response.json()
            df = pd.DataFrame.from_dict(weatherInfo['properties']['periods'])
    elif csvLocation.exists():
        df = pd.read_csv(csvLocation)

    return df



if __name__ == '__main__':
    csvLocation = CONFIG_PATH / CSV_NAME
    df = getWeatherData(csvLocation) 

    if df is not None:
        processWeatherData(df, csvLocation)
    else:
        print("N/A")
